import { expect } from 'chai'
import { currencyInfo } from '@/store/currency-info/currency-info'

const state = currencyInfo.state
const mutations = currencyInfo.mutations
const getters = currencyInfo.getters
const testCurrencies = [
  {
    id: 'ID1',
    name: 'NAME1',
    value: 1,
    desc: 'DESC1'
  },
  {
    id: 'ID2',
    name: 'NAME2',
    value: 2,
    desc: 'DESC2'
  },
]

describe('currency-info.ts', () => {
  describe('state', () => {
    it('should have the correct state', () => {
      expect(state).to.have.property('fetchingCurrencies')
      expect(state).to.have.property('currencies')
    })
  })
  
  describe('mutations', () => {
    it('should update the fetching status', () => {
      const expectedState = 'fetching'
      mutations?.setFetchingStatus(state, expectedState)
      expect(state.fetchingCurrencies).to.equal(expectedState)
    })

    it('should update the currencies', () => {
      mutations?.setCurrencies(state, testCurrencies)
      expect(state.currencies).to.equal(testCurrencies)  
    })
  })

  describe('getters', () => {
    it('should get the correct name depending on the ID', () => {
      expect(getters?.getName(state, getters, null, null)('ID1')).to.equal('NAME1')
      expect(getters?.getName(state, getters, null, null)('ID2')).to.equal('NAME2')
    })

    it('should get the correct description depending on the ID', () => {
      expect(getters?.getDesc(state, getters, null, null)('ID1')).to.equal('DESC1')
      expect(getters?.getDesc(state, getters, null, null)('ID2')).to.equal('DESC2')
    })
  })
})