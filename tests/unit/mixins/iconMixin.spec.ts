import { expect } from 'chai'
import iconMixin from '@/mixins/iconMixin.ts'

let classTable: any

describe('iconMixin.ts', () => {

  before(() => {
    classTable = {
      BTC: ['fab', 'bitcoin'],
      ETH: ['fab', 'ethereum'],
      EUR: ['fa', 'euro-sign'],
      GBP: ['fa', 'pound-sign'],
      USD: ['fa', 'dollar-sign'],
      YEN: ['fa', 'yen-sign'],
      RUB: ['fa', 'ruble-sign'],
      INR: ['fa', 'rupee-sign'],
      LRA: ['fa', 'lira-sign'],
      BCH: ['fab', 'btc'],
      XYZ: ['fa', 'coins']
    }
  })

  it('should return the correct class array according to input ID', () => {
    for(let key in classTable){
      expect(iconMixin.methods.getIconClass(key)).to.eql(classTable[key])
    }
  })

  it('should determine if a given string ID is a crypto ID', () => {
    const cryptoIDs = ['BTC', 'ETH', 'BCH']

    cryptoIDs.forEach(
      id => expect(iconMixin.methods.isCrypto(id)).to.be.true
    )

    const noCryptoID = 'ASD'
    expect(iconMixin.methods.isCrypto(noCryptoID)).to.be.false
  })
})