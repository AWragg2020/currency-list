import { expect } from 'chai'
import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import CurrencyDetailView from '@/views/CurrencyDetailView.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

let wrapper: any
let store: any
let stubActions = {
  fetchCurrencies: sinon.stub()
}

const testID = 'ID1'
const expectedName = 'NAME1'
const expectedDesc = 'DESC1'

let initState = {
  currencies: [
    {
      id: testID,
      name: expectedName,
      value: 0,
      desc: expectedDesc
    }
  ]
}

let testGetters = {
  getName: (state: any) => function(id: string) { return expectedName },
  getDesc: (state: any) => function(id: string) { return expectedDesc }
}

describe('CurrencyDetailView.vue', () => {

  beforeEach(() => {

    // Clear store before each test
    store = new Vuex.Store({
      modules: {
        currencyInfo: {
          namespaced: true,
          state: initState,
          actions: stubActions,
          getters: testGetters
        }
      }
    })

    // store, localVue,
    wrapper = shallowMount(CurrencyDetailView, { store, localVue, propsData: { id: testID } })
  })

  it('should call the store to get the list of currencies when mounted', () => {
    expect(stubActions.fetchCurrencies.called).to.be.true
  })

  it('should set its props array properly', () => {
    expect(wrapper.props().id).to.equal(testID)
  })

  it('should get the currency\'s name from the store', () => {
    expect(wrapper.find('.name').text()).to.equal(expectedName)
  })

  it('should get the currency\'s description from the store', () => {
    expect(wrapper.find('.description').text()).to.equal(expectedDesc)
  })

  it('should display 404 page when ID is not valid', () => {
    wrapper = shallowMount(CurrencyDetailView, { store, localVue, propsData: { id: 'NOTVALID' } })
    expect(wrapper.find('.icon').text()).to.equal('404')
    expect(wrapper.find('.description').text()).to.equal('The requested currency ID was not found: NOTVALID')
  })
})