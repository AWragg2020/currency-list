import { expect } from 'chai'
import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import sinon from 'sinon'
import * as _ from 'lodash'
import CurrencyList from '@/components/currency-list/CurrencyList.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

let wrapper: any
let testCurrencies: _.List<any> | null | undefined = []
let store: any
let initState: any
let stubActions = {
  fetchCurrencies: sinon.stub()
}
let stubMutations = {
  setCurrencies: sinon.stub()
}
let clock:any

describe('CurrencyList.vue', () => {

  beforeEach(() => {
    clock = sinon.useFakeTimers()

    testCurrencies = [
      {
        id: 'ID1',
        name: 'Name 1',
        value: 5,
        desc: 'Description 1'
      },
      {
        id: 'ID2',
        name: 'Name 2',
        value: 1,
        desc: 'Description 2'
      },
      {
        id: 'ID3',
        name: 'Name 3',
        value: 9,
        desc: 'Description 3'
      },
    ]

    // Clear store before each test
    initState = {
      currencies: testCurrencies
    }

    store = new Vuex.Store({
      modules: {
        currencyInfo: {
          namespaced: true,
          state: initState,
          mutations: stubMutations,
          actions: stubActions
        }
      }
    })

    wrapper = shallowMount(CurrencyList, { store, localVue })
  })

  it('should call the store to get the list of currencies when mounted', () => {
    expect(stubActions.fetchCurrencies.called).to.be.true
  })

  it('should poll the currency list correctly', () => {
    // Reset the action stub so it doens't remember previous calls
    stubActions.fetchCurrencies.resetHistory()
    clock.tick(wrapper.vm.millisecondsInterval)
    expect(stubActions.fetchCurrencies.called).to.be.true
  })

  it('should sort the currencies from highest value to lowest', () => {
    expect(wrapper.vm.sortedCurrencies).to.eql(_.orderBy(testCurrencies, 'value', 'desc'))
  })

  it('should render as many \'li\' html elements as items in the currencies array', () => {
    expect(wrapper.findAll('li').length).to.equal(testCurrencies?.length)
  })
})
