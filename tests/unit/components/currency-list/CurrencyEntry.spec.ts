import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import sinon from 'sinon'
import CurrencyEntry from '@/components/currency-list/CurrencyEntry.vue'

describe('CurrencyEntry.vue', () => {
  
  const classTable = {
    BTC: 'bitcoin',
    ETH: 'ethereum',
    EUR: 'euro-sign',
    GBP: 'pound-sign',
    USD: 'dollar-sign',
    YEN: 'yen-sign',
    RUB: 'ruble-sign',
    INR: 'rupee-sign',
    LRA: 'lira-sign',
    BCH: 'btc',
    EVC: 'coins'
  } as Record<string, any>

  const testName = 'Test currency'
  const testValue = 20
  const testDesc = 'Test description'
  
  it('should render the correct labels and values on the template', () => {
    const wrapper = shallowMount(CurrencyEntry, {
      propsData: {
        name: testName,
        value: testValue,
        desc: testDesc
      }
    })

    expect(wrapper.find('div.name').text()).to.equal(testName)
    expect(wrapper.find('div.value').text()).to.equal(testValue.toString())
    expect(wrapper.find('div.desc').text()).to.equal(testDesc)
  })

  it('should apply \'odd\' or \'even\' class according to its index', () => {
    const wrapper = shallowMount(CurrencyEntry)

    wrapper.setProps({index: 1})
    expect(wrapper.classes()).to.include('odd')

    wrapper.setProps({index: 2})
    expect(wrapper.classes()).to.include('even')
  })

  it('toDetail should call $router.push with the right options', () => {
    const testID = 'TEST'
    const $router = {
      push: sinon.stub()
    }

    const wrapper = shallowMount(CurrencyEntry, {
      propsData: {
        id: testID
      },
      mocks: {
        $router
      }
    })

    wrapper.find('section').trigger('click')
    expect($router.push.args[0]).to.eql(['/detail/'+testID])
  })
})
