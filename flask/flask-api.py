import os.path
from flask import Flask, escape, request, jsonify, make_response
import json
import random

SRC_DIR = os.path.abspath(os.path.dirname(__file__))
DATA_DIR = os.path.join(SRC_DIR, 'data')

app = Flask(__name__)

@app.route('/api/currencies.json')
def currencies():
    with open(DATA_DIR + '/currencies.json') as currencies_file:
        currencies = json.load(currencies_file)
        currency = random.choice(currencies)
        currency["value"] += random.randrange(1, 21, 1)

        with open(DATA_DIR + '/currencies.json', 'w') as outfile:
            json.dump(currencies, outfile)

        return make_response(jsonify(currencies), 200)

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', 'http://localhost:8080')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET')
  response.headers.add('Access-Control-Allow-Credentials', 'true')
  return response
