# Currency Index Table

## Description
List of currencies which displays, in each of its rows, the index, name, value and a shortened description of the currency. It calls the backend API every 5 seconds. With every call, a random currency is chosen to increase its value with a step of 1 to 20 (also chosen at random). The currencies are automatically sorted from highest value to lowest. If any of the entries are clicked, the SPA redirects to `/detail/:id` in order to show a full description of the currency. If a not valid ID is forced in the url, a `404 page` is displayed.

![Demo](/demo.gif)

## Project setup
```
npm install
```
## Run application
In order to run the application, the backend APi has to be up first:
```
npm run serve-api
```

Once that's done, the frontend can be run with:
```
npm run serve
```

### Run unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
