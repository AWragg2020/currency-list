import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBitcoin, faEthereum, faBtc } from '@fortawesome/free-brands-svg-icons'
import { faEuroSign, faPoundSign, faDollarSign, faYenSign, faRubleSign, faRupeeSign, faLiraSign, faCoins, faArrowRight } from '@fortawesome/free-solid-svg-icons'

library.add(
  faBitcoin,
  faEthereum,
  faBtc,
  faEuroSign,
  faPoundSign,
  faDollarSign,
  faYenSign,
  faRubleSign,
  faRupeeSign,
  faLiraSign,
  faCoins,
  faArrowRight
)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
