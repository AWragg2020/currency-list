import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import CurrencyListView from '@/views/CurrencyListView.vue'

Vue.use(VueRouter)

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'root',
    component: CurrencyListView
  },
  {
    path: '/detail/:id',
    name: 'detail',
    props: true,
    component: () => import('@/views/CurrencyDetailView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
