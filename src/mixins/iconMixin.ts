export default {
  methods: {
    getIconClass(id: string) {
      let iconClass
      switch(id) {
        case "BTC": iconClass = 'bitcoin';
          break;
        case "ETH": iconClass = 'ethereum';
          break;
        case "EUR": iconClass = 'euro-sign';
          break;
        case "GBP": iconClass = 'pound-sign';
          break;
        case "USD": iconClass = 'dollar-sign';
          break;
        case "YEN": iconClass = 'yen-sign';
          break;
        case "RUB": iconClass = 'ruble-sign';
          break;
        case "INR": iconClass = 'rupee-sign';
          break;
        case "LRA": iconClass = 'lira-sign';
          break;
        case "BCH": iconClass = 'btc';
          break;
        default: iconClass = 'coins';
          break;
      }
      return [this.isCrypto(id) ? 'fab' : 'fa', iconClass]
    },

    isCrypto(id: string): Boolean {
      return id === "BTC" || id === "ETH" || id === "BCH"
    }
  },
};