import Vue from 'vue'
import Vuex from 'vuex'
import { currencyInfo } from './currency-info/currency-info'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    currencyInfo
  }
})
