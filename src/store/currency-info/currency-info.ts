import { Module } from 'vuex'
import axios, { AxiosInstance, AxiosResponse } from 'axios'

const BASE_URL = 'http://localhost:5000'
const apiClient: AxiosInstance = axios.create()

const initState = {
  fetchingCurrencies: 'notFetching',
  currencies: [],
}

export const currencyInfo: Module<any, any> = {
  namespaced: true,
  state: initState,
  mutations: {
    setFetchingStatus(state, status: string) {
      state.fetchingCurrencies = status
    },

    setCurrencies(state, currencies) {
      state.currencies = currencies
    },
  },
  actions: {
    async fetchCurrencies({ commit }) {
      commit('setFetchingStatus', 'fetching')
      let response: AxiosResponse = await apiClient.get(BASE_URL + '/api/currencies.json')
      commit('setCurrencies', response?.data)
      commit('setFetchingStatus', 'notFetching')
    },
  },
  getters: {
    getName: state => (id: string) => {
      const currency = state.currencies.find(
        (e: any) => e["id"] === id
      )
      return currency?.["name"]
    },

    getDesc: state => (id: string) => {
      const currency = state.currencies.find(
        (e: any) => e["id"] === id
      )
      return currency?.["desc"]
    },
  },
}